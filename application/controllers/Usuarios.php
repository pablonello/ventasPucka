<?php

class Usuarios extends MY_Controller {

    public function __construct() {
        parent::__construct();

        /*
         * libreria para cargar el body del template en etodas las web html
         */
        $this->load->library('Form_validation');
        $this->load->helper('form');
    }

    public function index() {
        if ($this->session->userdata('logueado')) {
            redirect("Usuarios/list_usuarios");
        } else {
            show_404();
        }
    }

    /*
     * Listar Usuarios
     */

    public function list_usuarios() {
//      
//       
        $crud = new grocery_CRUD();                 //creo un nuevo objeto de la libreria CRUD
//        $crud->set_theme('datatables');             //selecciono el tema
        $crud->set_table('usuarios');              //selecciono la tabla
        $crud->set_subject('Usuarios');            //pongo el titulo al boton para agregar un registro nuevo
        $crud->columns('usuario', 'correo', 'tipo', 'nivel');    //pongo las columnas a mostrar

        $crud->callback_before_update(array($this, 'user_after_update_callback'));


        $crud->display_as('psw', 'Contraseña');
////
        //oculto campos
        $crud->field_type('fechaCreacion', 'hidden');
        $crud->field_type('fechaModificacion', 'hidden');
        $crud->field_type('banned', 'hidden');
        $crud->field_type('psw', 'hidden');

        $crud->unset_jquery();
        $crud->unset_clone();
        $crud->unset_read();
//        $crud->unset_edit();
        $crud->unset_add();

//        //accion editar
//        $crud->add_action('Editar', '', 'Usuarios/save_usuarios', 'edit-icon');


        $output = $crud->render(); //renderiso el tema en la pagina web

        $view["grocery_crud"] = json_encode($output);
        $view["title"] = "Lista de Usuarios";
        $this->parser->parse("template/body", $view);
    }

    /*     * ***
     * CRUD PARA LOS PROVEEDORES
     */

    public function save_usuarios($idusuario = null) {

        if ($idusuario == null) {

            $data['usuario'] = $data['psw'] = $data['tipo'] = $data['nivel'] = $data['correo'] = "";
            $view["title"] = "Crear Usuario";
        } else {

            // busco el usuario en mi base de datos
            $usuario = $this->Usuario->find($idusuario);
            //chequeo sino me ponen en la barra algun idusuario que no exista
            if (!isset($usuario)) {
                show_404();
            }


            //cargo los valores para modificarlos
            $data['usuario'] = $usuario->usuario;
            $data['psw'] = $usuario->psw;
            $data['tipo'] = $usuario->tipo;
            $data['nivel'] = $usuario->nivel;
            $data['correo'] = $usuario->correo;
            $view["title"] = "Actualizar Usuario";
        }



        if ($this->input->server('REQUEST_METHOD') == "POST") {

            $this->form_validation->set_rules('usuario', 'usuario', 'required');
            $this->form_validation->set_rules('psw', 'psw', 'required');


            $post = $this->input->post();
            $data['usuario'] = $this->input->post("usuario");

            //encripto el psw 
            $psw = $this->input->post("psw");
            $pswEncri = $this->Usuario->encryption($psw);

            if ($this->form_validation->run()) {

                if ($idusuario == null) {
//                    print '<pre>';
//                    print_r($post);
//                    print '</pre>';
//                    die();
                    $banned = 0;
                    $fechaCreacion = date('Y-m-d H:i:s');
                    $save = array(
                        'usuario' => $post['usuario'],
                        'psw' => $pswEncri,
                        'tipo' => $post['tipo'],
                        'nivel' => $post['nivel'],
                        'correo' => $post['correo'],
                        'banned' => $banned,
                        'fechaCreacion' => $fechaCreacion
                    );
                    $idusuario = $this->Usuario->insert($save);

                    if ($idusuario > 0) {
                        $this->session->set_flashdata('Usuario cargado', 'El usuario se cargo correctamente');
                        redirect('Usuarios/index');
                    } else {
                        $this->session->set_flashdata('Usuario error', 'El usuario No se pudo cargar correctamente');
                        redirect('Usuarios/index');
                    }
                } else {
                    $fechaModificacion = date('Y-m-d H:i:s');
                    $save = array(
                        'usuario' => $this->input->post("usuario"),
                        'psw' => $pswEncri,
                        'tipo' => $this->input->post("tipo"),
                        'nivel' => $this->input->post("nivel"),
                        'fechaModificacion' => $fechaModificacion
                    );
                    $result = $this->Usuario->update($idusuario, $save);
                    if ($result > 0) {
                        $this->session->set_flashdata('text', 'Se actualizo correctamente el Usuario');
                        $this->session->set_flashdata('type', 'success');
                        redirect('Usuarios/index');
                    } else {
                        $this->session->set_flashdata('text', 'No se pudo actualizar el Usuario');
                        $this->session->set_flashdata('type', 'danger');
                        redirect('Usuarios/index');
                    }
                }
            }
        }

        /*
         * ESTRUCUTRA PARA CONVERTIRLO A STRING SINO SE TIENE UN DATO EN EL SEGUNDO CAMPO SE PONE NULL
         */

        $view["body"] = $this->load->view("usuarios/save", $data, TRUE);
        $this->parser->parse("template/body", $view);
    }
 
    public function post_delete($usuarioid = null) {

        if ($usuarioid == null) {
            echo 0;
        } else {
            $this->Usuario->delete($usuarioid);
            echo 1;
            $this->session->set_flashdata('text', 'Se borro correctamente el Usuario');
            $this->session->set_flashdata('type', 'success');
        }
    }

    public function enviar_correos($usuarioid = null) {
        $this->email->send_email();
        $this->session->set_flashdata('text', 'Se envio el correo exitosamente');
        $this->session->set_flashdata('type', 'success');
    }

//    function user_before_insert_callback($post_array) {
//
//        $post_array['psw'] = $this->Usuario->encryption($post_array['psw']);
//        $post_array['fechaCreacion'] = date('Y-m-d H:i:s');
//        $post_array['banned'] = 0;
//        return $post_array;
//    }

    function user_after_update_callback($post_array) {
        $post_array['usuario'] = $post_array['usuario'];
        $post_array['psw'] = $this->Usuario->encryption($post_array['psw']);
        $post_array['tipo'] = $post_array['tipo'];
        $post_array['nivel'] = $post_array['nivel'];
        $post_array['correo'] = $post_array['correo'];
        $post_array['fechaModificacion'] = date('Y-m-d H:i:s');
//        $post_array['banned'] = 0;
        return $post_array;
    }

}
