<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo APP_NAME . ' | ' . APP_DESCRIPTION ?></title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/font-awesome/css/font-awesome.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/admin/AdminLTE.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/admin/custom.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/Ionicons/css/ionicons.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/admin/skins/_all-skins.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

        <script src="<?php echo base_url() ?>assets/js/jquery-3.3.1.min.js"></script>

        <?php (isset($grocery_crud)) ? $this->load->view("template/grocery_crud_header", ["grocery_crud" => $grocery_crud]) : '' ?>

    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">


            <?php $this->load->view("template/header"); ?>
            <?php $this->load->view("template/nav"); ?>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <!-- CABERA DE LA PAGINA -->
                <section class="content-header">
                    <h1>
                        {title}
                    </h1>
                    <!-- GUIA DE DONDE ESTAMOS PARADOS EN LA WEB -->
                    <?php (isset($breadcrumb)) ? $this->load->view("template/breadcrumb", ["breadcrumb" => $breadcrumb]) : '' ?>
                </section>


                <!-- Main content -->
                <!-- CONTENIDO CENTRAL DE LA PAGINA -->
                <section class="content">

                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="box">
                            <div class="box-header">

                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <?php
                                if ($error = $this->session->flashdata('Empleado cargado')) {
                                    ?>
                                    <div class="row">
                                        <div >
                                            <div class="alert alert-dismissible alert-success">
                                                <?php
                                                echo $error;
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php } else if ($error = $this->session->flashdata('Empleado error')) { ?>
                                    <div class="row">
                                        <div >
                                            <div class="alert alert-dismissible alert-danger">
                                                <?php echo $error ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php } else if ($error = $this->session->flashdata('Empleado modificado')) { ?>
                                    <div class="row">
                                        <div >
                                            <div class="alert alert-dismissible alert-success">
                                                <?php echo $error ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php } else if ($error = $this->session->flashdata('Empleado modificado error')) { ?>
                                    <div class="row">
                                        <div >
                                            <div class="alert alert-dismissible alert-danger">
                                                <?php echo $error ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php if (isset($body)): ?>
                                    {body}
                                <?php endif; ?>

                                <?php (isset($grocery_crud)) ? $this->load->view("template/grocery_crud", ["grocery_crud" => $grocery_crud]) : '' ?>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <!-- /.box-body -->         
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <?php $this->load->view("template/footer"); ?>     
        </div>
        <!-- ./wrapper -->


        <script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/jquery.toaster.js"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo base_url() ?>assets/js/admin/adminlte.min.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="<?php echo base_url() ?>assets/ckeditor/ckeditor.js"></script>
        <script src="<?php echo base_url() ?>assets/js/admin/demo.js"></script>
        <script src="<?php echo base_url() ?>assets/js/admin/main.js"></script>



    </body>
</html>