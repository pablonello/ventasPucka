<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar" style="height: auto;">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?php echo base_url() ?>assets/img/logo.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?php echo $this->session->userdata('nombre') ?></p>
            </div>
        </div>

        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu tree" data-widget="tree">


            <?php if ($this->session->userdata('nivel') == 10) { ?>
                <li class="header">MENU DE NAVGACION</li>

                <li>
                    <a href="<?php echo base_url() ?>Admin"><i class="fa fa-dashboard"></i> Home</a>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-user"></i>
                        <span>Opciones de Usuarios</span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url() ?>Usuarios"><i class="fa fa-circle-o"></i> Lista de usuarios</a></li>
                        <li><a href="<?php echo base_url() ?>Usuarios/save_usuarios"><i class="fa fa-circle-o"></i> Nuevo Usuario</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-address-card"></i>
                        <span>Opciones de Productos</span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url() ?>Productos"><i class="fa fa-circle-o"></i> Listar productos</a></li>
                        <li><a href="<?php echo base_url() ?>Productos/list_productos/add"><i class="fa fa-circle-o"></i> Nuevo producto</a></li>
                    </ul>
                </li>
                <li>
                    <a href="<?php echo base_url() ?>CalcularCuotas"><i class="fa fa-calculator"></i>Calcular Cuota</a>
                </li>
                <li>
                    <a href="<?php echo base_url() ?>Emails"><i class="fa fa-ticket"></i>Ticket</a>
                </li>


            <?php } else if ($this->session->userdata('nivel') == 5) { ?>
                <li class="header">MENU DE NAVGACION</li>
                <li>
                    <a href="<?php echo base_url() ?>Admin"><i class="fa fa-dashboard"></i> Home</a>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-address-card"></i>
                        <span>Opciones de Productos</span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url() ?>Productos"><i class="fa fa-circle-o"></i> Listar productos</a></li>
                        <li><a href="<?php echo base_url() ?>Productos/list_productos/add"><i class="fa fa-circle-o"></i> Nuevo producto</a></li>
                    </ul>
                </li>
                <!-- <li>
                    <a href="<?php echo base_url() ?>CalcularCuotas"><i class="fa fa-ticket"></i>Calcular Cuota</a>
                </li>
                <li>
                    <a href="<?php echo base_url() ?>Emails"><i class="fa fa-ticket"></i>Ticket</a>
                </li> -->

            <?php } else if ($this->session->userdata('nivel') == 0) { ?>
                <li class="header">MENU DE NAVGACION</li>
                <li>
                    <a href="<?php echo base_url() ?>Admin"><i class="fa fa-dashboard"></i> Home</a>
                </li>
                <li>
                    <a href="<?php echo base_url() ?>CalcularCuotas"><i class="fa fa-ticket"></i>Calcular Cuota</a>
                </li>
                <li>
                    <a href="<?php echo base_url() ?>Emails"><i class="fa fa-ticket"></i>Ticket</a>
                </li>
            <?php } ?>

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>